# frozen_string_literal: true

class MessageDecorator < ApplicationDecorator
  delegate_all

  def display_preview_body
    h.strip_tags(body).truncate(75, separator: '...')
  end

  def prepare_for_cable
    attributes.merge(html: html)
  end

  def html
    h.render '/users/messages/message', message: self
  end

  def display_group_class(group)
    can_display_group?(group) ? 'group' : ''
  end

  def display_remove_link
    if h.has_active_subscription?
      klass = 'has_subscription'
      tooltip = 'Удалить сообщение<br> у всех?'
      confirm_delete = true
      url = h.users_conversation_message_url(model.conversation, model, format: :js)
    else
      klass = 'no_subscription'
      tooltip = "Активируйте #{Config.site_name} Plus, чтобы удалять сообщения из чата"
      confirm_delete = false
      url = 'javascript:;'
    end

    h.link_to url,
              method: :delete,
              class: klass,
              title: tooltip,
              data: {
                toggle: 'message-tooltip',
                placement: 'bottom',
                confirm_delete: confirm_delete,
                question: 'Уверены?'
              },
              'data-confirm_text' => 'Удалить', 'data-deny_text' => 'Отмена' do
      ''
    end
  end

  private

  def can_display_group?(group)
    (h.has_active_subscription? && group.present? && model.read?) || group.present?
  end
end
