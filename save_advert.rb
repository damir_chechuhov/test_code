# frozen_string_literal: true

module Adverts
  class SaveAdvert
    include Interactor

    delegate :advert, to: :context

    def call
      context.fail!(errors: 'Invalid state') unless Advert.states[state]
      context.fail!(errors: form.errors) unless form.validate(params)

      backup(advert)

      form.save do
        model = form.model
        context.state_was = advert.state
        context.already_waiting_publication = WaitingPublicationAdvertSpecification.new(advert).satisfied?

        model.assign_attributes(params.merge(send("#{state}_params")))
        form.save!
        context.model = model
      end
    end

    def rollback
      if @persisted
        advert.update(@advert_backup)
      else
        advert.destroy
      end
    end

    private

    def backup(advert)
      @advert_backup = advert.attributes.merge(tag_list: advert.tag_list)
      @persisted = advert.persisted?
    end

    def form
      @form ||= "Advert#{state.titleize}Form".constantize.new(advert)
    end

    def state
      params[:state]
    end

    def advert_promotion
      params[:advert_promotion]
    end

    def date_late_publication
      params[:date_late_publication]
    end

    def draft_params
      { state: :draft }
    end

    def published_params
      return moderation_params if need_moderation?

      { publish_at: published_publish_at }.merge(up_to_at_params)
    end

    def moderation_params
      # TODO
      # Implement the process of UP TO if the advert is moderated
      { state: :moderation, publish_at: moderation_publish_at }.merge(up_to_at_params)
    end

    def up_to_at_params
      return {} if advert_promotion.blank?

      hours = advert_promotion_hours[advert_promotion.to_sym].hours

      up_to_at = publish_later? ? (Time.parse(date_late_publication) + hours) : hours.from_now
      up_to_updated_at = publish_later? ? date_late_publication : Time.current

      { up_to_at: up_to_at, up_to_updated_at: up_to_updated_at }
    end

    def moderation_publish_at
      publish_later? ? date_late_publication : advert.publish_at
    end

    def published_publish_at
      publish_later? ? date_late_publication : parse_publish_at
    end

    def parse_publish_at
      advert.publish_at.nil? ? Time.current : advert.publish_at
    end

    def need_moderation?
      advert.user.violator? || StopWordsAdvertSpecification.new(form).satisfied?
    end

    def publish_later?
      params[:publish_later].present?
    end

    def advert_promotion_hours
      {
        advert_promotion_3_hours: 3,
        advert_promotion_12_hours: 12,
        advert_promotion_24_hours: 24,
        advert_promotion_72_hours: 72
      }
    end

    def params
      return @params if @params.present?

      @params = context.params
      @params[:tag_list] = @params[:tag_list].to_s.gsub(/#/, '') if @params.has_key?(:tag_list)

      @params
    end
  end
end
