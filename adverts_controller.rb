# frozen_string_literal: true

class Users::AdvertsController < ApplicationController
  before_action :store_location!
  before_action :authenticate_user!
  before_action :set_advert, only: %i[edit update destroy unpublish up_to]
  before_action :set_new_advert, only: %i[new create]
  layout :set_app_layout, only: %i[new edit]

  def index
    @page_title = "#{Config.site_name} - Мои объявления"
    @adverts = NotDestroyedAdvertSpecification.to_scope.merge(current_user.adverts.order_by_created)
    @published = @adverts.merge(PublishedAdvertSpecification.to_scope).decorate
    @waiting_publication = @adverts.merge(WaitingPublicationAdvertSpecification.to_scope).decorate
  end

  def new
    render plain: current_user.decorate.display_state, status: 401 unless policy([:users, @advert]).new?
    @page_title = "#{Config.site_name} - Новое объявление"
  end

  def edit
    authorize([:users, @advert], :edit?)
    @page_title = "#{Config.site_name} - Редактирование объявления"
  end

  def create
    authorize([:users, @advert], :create?)

    result = Adverts::Save.call(advert: @advert, params: advert_params.merge(state: params[:commit]))
    respond_to do |form|
      if result.success?
        form.html { redirect_to(users_adverts_path) }
        form.json { render json: { advert: @advert, state: @advert.display_state }, status: :ok }
      else
        form.html do
          @advert.assign_attributes(advert_params)
          render(:new)
        end
        form.json do
          render json: { messages: result.errors&.messages, message: result.message },
                 status: :unprocessable_entity
        end
      end
    end
  end

  def update
    authorize([:users, @advert], :update?)

    result = Adverts::Save.call(advert: @advert, params: advert_params.merge(state: params[:commit]))
    respond_to do |form|
      if result.success?
        form.html { redirect_to(users_adverts_path) }
        form.json { render json: { advert: @advert, state: @advert.display_state }, status: :ok }
      else
        form.html do
          @advert.assign_attributes(advert_params)
          render(:edit)
        end
        form.json do
          render json: { messages: result.errors&.messages, message: result.message },
                 status: :unprocessable_entity
        end
      end
    end
  end

  def destroy
    Adverts::Destroy.call(advert: @advert)
    redirect_to(users_adverts_path)
  end

  def unpublish
    @advert.draft!
    redirect_to(users_adverts_path(state: :draft))
  end

  def up_to
    @result = Adverts::Save.call(advert: @advert, params: advert_up_to_params.merge(state: 'published'))
  end

  private

  def advert_params
    params.require(:advert).permit(:title, :body, :tag_list, :city_id,
                                   :price_cents, :date_late_publication,
                                   :publish_later, :advert_promotion, :highlight,
                                   photos_attributes: %i[id image image_cache _destroy])
  end

  def advert_up_to_params
    params.require(:advert).permit(:advert_promotion, :highlight)
  end

  def set_advert
    @advert = current_user.adverts.find(params[:id]).decorate
  end

  def set_new_advert
    @advert = current_user.adverts.build.decorate
  end
end
